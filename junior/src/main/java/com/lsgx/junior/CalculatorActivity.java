package com.lsgx.junior;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.junior.util.ArithUtil;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = "CalculatorActivity";
    private TextView tv1_calc_result; // 声明一个文本视图对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        tv1_calc_result = findViewById(R.id.tv1_calc_result);
        tv1_calc_result.setMovementMethod(new ScrollingMovementMethod());

        findViewById(R.id.btn1_calc_ce).setOnClickListener(this);
        findViewById(R.id.btn2_calc_ac).setOnClickListener(this);
        findViewById(R.id.btn3_calc_add).setOnClickListener(this);
        findViewById(R.id.btn4_calc_sub).setOnClickListener(this);
        findViewById(R.id.btn5_calc_seven).setOnClickListener(this);
        findViewById(R.id.btn6_calc_eight).setOnClickListener(this);
        findViewById(R.id.btn7_calc_nine).setOnClickListener(this);
        findViewById(R.id.btn8_calc_mul).setOnClickListener(this);
        findViewById(R.id.btn9_calc_four).setOnClickListener(this);
        findViewById(R.id.btn10_calc_five).setOnClickListener(this);
        findViewById(R.id.btn11_calc_six).setOnClickListener(this);
        findViewById(R.id.btn12_calc_div).setOnClickListener(this);
        findViewById(R.id.btn13_calc_one).setOnClickListener(this);
        findViewById(R.id.btn14_calc_two).setOnClickListener(this);
        findViewById(R.id.btn15_calc_three).setOnClickListener(this);
        findViewById(R.id.btn16_calc_sqrt).setOnClickListener(this);
        findViewById(R.id.btn17_calc_zero).setOnClickListener(this);
        findViewById(R.id.btn18_calc_dot).setOnClickListener(this);
        findViewById(R.id.btn19_calc_equal).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        String inputText = ((TextView) v).getText().toString();
        switch (viewId)
        {
            case R.id.btn2_calc_ac :
            {
                clearCalcResult("0");
                break;
            }
            case R.id.btn1_calc_ce :
            {
                if (operator.equals("")) {
                    if (firstNum.length() == 1) {
                        firstNum = "0";
                    } else if (firstNum.length() > 0) {
                        firstNum = firstNum.substring(0, firstNum.length() - 1);
                    } else {
                        Toast.makeText(this, "没有可取消的数字了", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    showText = firstNum;
                    tv1_calc_result.setText(showText);
                } else {
                    if (nextNum.length() == 1) {
                        nextNum = "0";
                    } else if (nextNum.length() > 0) {
                        nextNum = nextNum.substring(0, nextNum.length() - 1);
                    } else {
                        Toast.makeText(this, "没有可取消的数字了", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    showText = showText.substring(0, showText.length() - 1);
                    tv1_calc_result.setText(showText);
                }
                break;
            }
            case R.id.btn19_calc_equal :
            {
                if (operator.length() == 0 || operator.equals(((TextView) findViewById(R.id.btn19_calc_equal)).getText().toString())) {
                    Toast.makeText(this, "请输入运算符", Toast.LENGTH_SHORT).show();
                    return;
                } else if (nextNum.length() <= 0) {
                    Toast.makeText(this, "请输入数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (execCalcResult()) { // 计算成功，则显示计算结果
                    operator = inputText;
                    showText = showText + "=" + result;
                    tv1_calc_result.setText(showText);
                } else { // 计算失败，则直接返回
                    return;
                }
                break;
            }
            case R.id.btn3_calc_add :
            case R.id.btn4_calc_sub :
            case R.id.btn8_calc_mul :
            case R.id.btn12_calc_div :
            {
                if (firstNum.length() <= 0) {
                    Toast.makeText(this, "请输入数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (operator.length() == 0
                        || operator.equals(((TextView) findViewById(R.id.btn19_calc_equal)).getText().toString())
                        || operator.equals(((TextView) findViewById(R.id.btn16_calc_sqrt)).getText().toString())) {
                    operator = inputText; // 操作符
                    showText = showText + operator;
                    tv1_calc_result.setText(showText);
                } else {
                    Toast.makeText(this, "请输入数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            }
            case R.id.btn16_calc_sqrt :
            {
                if (firstNum.length() <= 0) {
                    Toast.makeText(this, "请输入数字", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (Double.parseDouble(firstNum) < 0) {
                    Toast.makeText(this, "开根号的数值不能小于0", Toast.LENGTH_SHORT).show();
                    return;
                }
                // 进行开根号运算
                result = String.valueOf(Math.sqrt(Double.parseDouble(firstNum)));
                firstNum = result;
                nextNum = "";
                operator = inputText;
                showText = showText + "√=" + result;
                tv1_calc_result.setText(showText);
                Log.w(TAG, "result = " + result + ", firstNum = " + firstNum + ", operator = " + operator);
                break;
            }

            case R.id.btn5_calc_seven :
            case R.id.btn6_calc_eight :
            case R.id.btn7_calc_nine :
            case R.id.btn9_calc_four :
            case R.id.btn10_calc_five :
            case R.id.btn11_calc_six :
            case R.id.btn13_calc_one :
            case R.id.btn14_calc_two :
            case R.id.btn15_calc_three :
            case R.id.btn17_calc_zero :
            case R.id.btn18_calc_dot :
            {
                if (operator.equals(((TextView) findViewById(R.id.btn19_calc_equal)).getText().toString())) { // 上一次点击了等号按钮，则清空操作符
                    operator = "";
                    firstNum = "";
                    showText = "";
                }
                if (viewId == R.id.btn18_calc_dot) { // 点击了小数点
                    inputText = ((TextView) findViewById(R.id.btn18_calc_dot)).getText().toString();
                }
                if (operator.equals("")) { // 无操作符，则继续拼接前一个操作数
                    if (firstNum.contains(((TextView) findViewById(R.id.btn18_calc_dot)).getText().toString())
                            && inputText.equals(((TextView) findViewById(R.id.btn18_calc_dot)).getText().toString())) {
                        return; // 一个数字不能有两个小数点
                    }
                    firstNum = firstNum + inputText;
                } else { // 有操作符，则继续拼接后一个操作数
                    if (nextNum.contains(((TextView) findViewById(R.id.btn18_calc_dot)).getText().toString())
                            && inputText.equals(((TextView) findViewById(R.id.btn18_calc_dot)).getText().toString())) {
                        return; // 一个数字不能有两个小数点
                    }
                    nextNum = inputText;
                }
                showText = showText + inputText;
                tv1_calc_result.setText(showText);
                break;
            }
            default :
            {
                Log.w(TAG, "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }

    private String operator = ""; // 操作符
    private String firstNum = ""; // 前一个操作数
    private String nextNum = ""; // 后一个操作数
    private String result = ""; // 当前的计算结果
    private String showText = ""; // 显示的文本内容

    // 开始加减乘除四则运算，计算成功则返回true，计算失败则返回false
    private boolean execCalcResult() {
        if (operator.equals(((TextView) findViewById(R.id.btn3_calc_add)).getText().toString())) {
            result = String.valueOf(ArithUtil.add(firstNum, nextNum));
        } else if (operator.equals(((TextView) findViewById(R.id.btn4_calc_sub)).getText().toString())) {
            result = String.valueOf(ArithUtil.sub(firstNum, nextNum));
        } else if (operator.equals(((TextView) findViewById(R.id.btn8_calc_mul)).getText().toString())) {
            result = String.valueOf(ArithUtil.mul(firstNum, nextNum));
        } else if (operator.equals(((TextView) findViewById(R.id.btn12_calc_div)).getText().toString())) {
            if (Double.parseDouble(nextNum) == 0) {
                Toast.makeText(this, "被除数不能为零", Toast.LENGTH_SHORT).show();
                return false;
            }
            result = String.valueOf(ArithUtil.div(firstNum, nextNum));
        } else {
            Log.w(TAG, "unknown operator is : " + ((TextView) findViewById(R.id.btn12_calc_div)).getText().toString());
        }
        Log.w(TAG, "calc result is : " + result);
        firstNum = result;
        nextNum = "";
        return true;
    }

    // 清空并初始化
    private void clearCalcResult(String text) {
        showText = text;
        tv1_calc_result.setText(showText);
        operator = "";
        firstNum = "";
        nextNum = "";
        result = "";
    }
}
