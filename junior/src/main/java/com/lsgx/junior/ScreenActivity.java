package com.lsgx.junior;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.junior.util.DensityUtil;

public class ScreenActivity extends AppCompatActivity {

    private TextView tv1_screen; // 声明一个文本视图对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);

        tv1_screen = findViewById(R.id.tv1_screen);
        showScreenInfo();
    }

    /**
     * 显示当前手机的屏幕参数信息
     */
    private void showScreenInfo() {
        // 获取手机屏幕的宽度
        int width = DensityUtil.getScreenWidth(this);
        // 获取手机屏幕的高度
        int height = DensityUtil.getScreenHeight(this);
        // 获取手机屏幕的像素密度
        float density = DensityUtil.getScreenDensity(this);
        // 拼接屏幕参数信息的内容文本
        String info = String.format("当前屏幕参数信息 \n宽度: %dpx \n高度: %dpx \n像素密度: %f \n",
                width, height, density);
        // 设置文本视图tv1_screen的文本内容
        tv1_screen.setText(info);
    }
}
