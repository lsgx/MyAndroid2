package com.lsgx.junior;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ColorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        TextView tv3_color_six = findViewById(R.id.tv3_color_six);
        // 六位十六进制数值中, 默认颜色的透明度为全透明, 所以不显示背景颜色
        //tv3_color_six.setBackgroundColor(0x3079FF);
        /**
         * 设置颜色值的方式
         * 直接设置颜色数值 0x993079FF
         * 调用此方法不带透明度值 int rgb(int, int, int)
         * 调用此方法带透明值 int argb(int, int, int, int)
         */
        tv3_color_six.setBackgroundColor(Color.rgb(0x30, 0x79, 0xFF));

        TextView tv4_color_eight = findViewById(R.id.tv4_color_eight);
        // 八位十六进制数值中, 最高的两位指透明度, FF为不透明 00为全透明(即白色)
        tv4_color_eight.setBackgroundColor(0x993079FF);
    }
}
