package com.lsgx.junior;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.junior.util.DensityUtil;

/**
 * 测试像素尺寸的转换
 */
public class PxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_px);
        // 将10dp的尺寸大小转换为对应的px数值
        int dip_10 = DensityUtil.dip2px(this, 10F);
        // 在调式控制台打印日志消息
        Log.w("PxActivity", "DensityUtil.dip2px : " + "10dp = " + dip_10 + "px");
        // 从布局文件中获取名叫tv3_padding的文本视图
        TextView tv_padding = findViewById(R.id.tv3_padding);
        // 设置该文本视图的内部文字与控件四周的间隔大小
        tv_padding.setPadding(dip_10, dip_10, dip_10, dip_10);
    }
}
