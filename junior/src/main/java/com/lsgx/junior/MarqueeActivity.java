package com.lsgx.junior;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MarqueeActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv1_marquee; // 声明一个文本视图对象
    private boolean isPaused = false; // 跑马灯文字是否暂停滚动

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marquee);

        // 从布局文件中获取名叫tv1_marquee的文本视图
        tv1_marquee = findViewById(R.id.tv1_marquee);
        // 给tv1_marquee设置点击监听器
        tv1_marquee.setOnClickListener(this);
        tv1_marquee.requestFocus(); // 强制获得焦点，让跑马灯滚起来
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv1_marquee) { // 点击了文本视图tv_marquee
            isPaused = !isPaused;
            // 在暂停时不允许获取焦点, 非暂停时允许获得焦点
            tv1_marquee.setFocusable(!isPaused);
            // 在暂停时不允许在触摸时获得焦点, 非暂停时允许在触摸时获得焦点
            tv1_marquee.setFocusableInTouchMode(!isPaused);
            if (isPaused) {
                tv1_marquee.requestFocus(); // 强制获得焦点，让跑马灯滚起来
            }
        }
    }
}
