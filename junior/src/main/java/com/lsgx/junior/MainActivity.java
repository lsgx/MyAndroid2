package com.lsgx.junior;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_chapter_2_1_demo_px).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_1_demo_color).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_1_demo_screen).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_2_demo_margin).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_2_demo_gravity).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_2_demo_scroll).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_marquee).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_bbs).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_click).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_scale).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_capture).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_3_demo_icon).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_4_demo_state).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_4_demo_shape).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_4_demo_nine).setOnClickListener(this);
        findViewById(R.id.btn_chapter_2_5_project_calculator).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId)
        {
            case R.id.btn_chapter_2_1_demo_px :
            {
                Intent intent = new Intent(this, PxActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_1_demo_color :
            {
                Intent intent = new Intent(this, ColorActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_1_demo_screen :
            {
                Intent intent = new Intent(this, ScreenActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_2_demo_margin :
            {
                Intent intent = new Intent(this, MarginActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_2_demo_gravity :
            {
                Intent intent = new Intent(this, GravityActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_2_demo_scroll :
            {
                Intent intent = new Intent(this, ScrollActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_marquee :
            {
                Intent intent = new Intent(this, MarqueeActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_bbs :
            {
                Intent intent = new Intent(this, BbsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_click :
            {
                Intent intent = new Intent(this, ClickActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_scale :
            {
                Intent intent = new Intent(this, ScaleActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_capture :
            {
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_3_demo_icon :
            {
                Intent intent = new Intent(this, IconActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_4_demo_state :
            {
                Intent intent = new Intent(this, StateActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_4_demo_shape :
            {
                Intent intent = new Intent(this, ShapeActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_4_demo_nine :
            {
                Intent intent = new Intent(this, NineActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_2_5_project_calculator :
            {
                Intent intent = new Intent(this, CalculatorActivity.class);
                startActivity(intent);
                break;
            }
             default :
             {
                 Log.w("MainActivity", "In click event, unknown view id is : " + viewId);
                 break;
             }
        }
    }
}
