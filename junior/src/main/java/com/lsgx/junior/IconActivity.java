package com.lsgx.junior;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class IconActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn1_icon_content; // 声明一个按钮对象
    private Drawable drawable; // 声明一个图形对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon);
        // 从布局文件中获取名叫btn1_icon_content的按钮控件
        btn1_icon_content = findViewById(R.id.btn1_icon_content);
        // 从资源文件ic_launcher.png中获取图形对象
        drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher);
        // 设置图形对象的矩形边界大小，注意必须设置图片大小，否则不会显示图片
        // 第一个0是距左边距离，第二个0是距上边距离，48分别是长宽
        // setBounds(left,top,right,bottom)里的参数从左到右分别是
        // drawable的左边到textview左边缘+padding的距离，drawable的上边离textview上边缘+padding的距离
        // drawable的右边边离textview左边缘+padding的距离，drawable的下边离textview上边缘+padding的距离
        // 所以right-left = drawable的宽，top - bottom = drawable的高
        drawable.setBounds(0, 0, 42, 42);
        // 设置按钮控件btn1_icon_content内部文字左边的图标
        btn1_icon_content.setCompoundDrawables(drawable, null, null, null);
        // 下面通过四个按钮，分别演示左、上、右、下四个方向的图标效果
        findViewById(R.id.btn2_icon_left).setOnClickListener(this);
        findViewById(R.id.btn3_icon_top).setOnClickListener(this);
        findViewById(R.id.btn4_icon_right).setOnClickListener(this);
        findViewById(R.id.btn5_icon_bottom).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) { // 一旦监听到点击动作，就触发监听器的onClick方法
        int viewId = v.getId();
        switch (viewId)
        {
            case R.id.btn2_icon_left :
            {
                // 设置按钮控件btn1_icon_content内部文字左边的图标
                btn1_icon_content.setCompoundDrawables(drawable, null, null, null);
                break;
            }
            case R.id.btn3_icon_top :
            {
                // 设置按钮控件btn1_icon_content内部文字上方的图标
                btn1_icon_content.setCompoundDrawables(null, drawable, null, null);
                break;
            }
            case R.id.btn4_icon_right :
            {
                // 设置按钮控件btn1_icon_content内部文字右边的图标
                btn1_icon_content.setCompoundDrawables(null, null, drawable, null);
                break;
            }
            case R.id.btn5_icon_bottom :
            {
                // 设置按钮控件btn1_icon_content内部文字下方的图标
                btn1_icon_content.setCompoundDrawables(null, null, null, drawable);
                break;
            }
            default :
            {
                Log.w("IconActivity", "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }
}
