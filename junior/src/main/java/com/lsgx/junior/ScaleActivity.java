package com.lsgx.junior;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ScaleActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv1_scale;  // 声明一个图像视图的对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale);

        // 从布局文件中获取名叫iv1_scale的图像视图
        iv1_scale = findViewById(R.id.iv1_scale);
        // 下面通过七个按钮，分别演示不同拉伸类型的图片拉伸效果
        findViewById(R.id.btn1_scale_fitCenter).setOnClickListener(this);
        findViewById(R.id.btn2_scale_centerCrop).setOnClickListener(this);
        findViewById(R.id.btn3_scale_centerInside).setOnClickListener(this);
        findViewById(R.id.btn4_scale_center).setOnClickListener(this);
        findViewById(R.id.btn5_scale_fitXY).setOnClickListener(this);
        findViewById(R.id.btn6_scale_fitStart).setOnClickListener(this);
        findViewById(R.id.btn7_scale_fitEnd).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) { // 一旦监听到点击动作，就触发监听器的onClick方法
        int viewId = v.getId();
        switch (viewId)
        {
            case R.id.btn1_scale_fitCenter :
            {
                // 将拉伸类型设置为“保持宽高比例，拉伸图片使其位于视图中间”
                iv1_scale.setScaleType(ImageView.ScaleType.FIT_CENTER);
                break;
            }
            case R.id.btn2_scale_centerCrop :
            {
                // 将拉伸类型设置为“拉伸图片使其充满视图，并位于视图中间”
                iv1_scale.setScaleType(ImageView.ScaleType.CENTER_CROP);
                break;
            }
            case R.id.btn3_scale_centerInside :
            {
                // 将拉伸类型设置为“保持宽高比例，缩小图片使之位于视图中间（只缩小不放大）”
                iv1_scale.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                break;
            }
            case R.id.btn4_scale_center :
            {
                // 将拉伸类型设置为“按照原尺寸居中显示”
                iv1_scale.setScaleType(ImageView.ScaleType.CENTER);
                break;
            }
            case R.id.btn5_scale_fitXY :
            {
                // 将拉伸类型设置为“拉伸图片使其正好填满视图（图片可能被拉伸变形）”
                iv1_scale.setScaleType(ImageView.ScaleType.FIT_XY);
                break;
            }
            case R.id.btn6_scale_fitStart :
            {
                // 将拉伸类型设置为“保持宽高比例，拉伸图片使其位于视图上方或左侧”
                iv1_scale.setScaleType(ImageView.ScaleType.FIT_START);
                break;
            }
            case R.id.btn7_scale_fitEnd :
            {
                // 将拉伸类型设置为“保持宽高比例，拉伸图片使其位于视图下方或右侧”
                iv1_scale.setScaleType(ImageView.ScaleType.FIT_END);
                break;
            }
            default :
            {
                Log.w("ScaleActivity", "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }

}
