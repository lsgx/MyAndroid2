package com.lsgx.junior;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.junior.util.DateUtil;

public class BbsActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnLongClickListener {

    private TextView tv1_bbs_control; // 声明一个文本视图对象
    private TextView tv2_bbs_content; // 声明一个文本视图对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbs);
        // 从布局文件中获取名叫tv1_bbs_control的文本视图
        tv1_bbs_control = findViewById(R.id.tv1_bbs_control);
        // 给tv1_bbs_control设置点击监听器
        tv1_bbs_control.setOnClickListener(this);
        // 给tv1_bbs_control设置长按监听器
        tv1_bbs_control.setOnLongClickListener(this);

        // 从布局文件中获取名叫tv2_bbs_content的文本视图
        tv2_bbs_content = findViewById(R.id.tv2_bbs_content);
        // 给tv2_bbs_content设置点击监听器
        tv2_bbs_content.setOnClickListener(this);
        // 给tv2_bbs_content设置长按监听器
        tv2_bbs_content.setOnLongClickListener(this);

        // 设置tv2_bbs_content内部文字的对齐方式为靠左且靠下
        //tv2_bbs_content.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        // 设置tv2_bbs_content为非单行模式
        //tv2_bbs_content.setSingleLine(false);
        // 设置tv2_bbs_content高度为八行文字那么高
        //tv2_bbs_content.setLines(8);
        // 设置tv2_bbs_content最多显示八行文字
        //tv2_bbs_content.setMaxLines(6);

        // 设置tv2_bbs_content内部文本的移动方式为滚动形式
        // 如果不设置将无法拉动文本
        tv2_bbs_content.setMovementMethod(new ScrollingMovementMethod());
    }

    private String[] mStrArray = {
            "你吃饭了吗？",
            "今天天气真好呀。",
            "我中奖啦！",
            "我们去看电影吧",
            "晚上干什么好呢？",
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv1_bbs_control || v.getId() == R.id.tv2_bbs_content) {
            // 生成一个0到4之间的随机数
            int number = (int) (Math.random() * 10) % 5;
            // 拼接聊天的文本内容
            // tv2_bbs_content.toString() 原始内容
            // DateUtil.getNowTime() 当前时间字符串
            // mStrArray[number] 随机字符串
            String newStr = String.format("%s\n%s %s",
                    tv2_bbs_content.getText().toString(), DateUtil.getNowTime(), mStrArray[number]);
            // 在调试控制台中打印拼接的字符串内容
            Log.w("BbsActivity", "string content : " + newStr);
            // 设置文本视图tv2_bbs_content的文本内容
            tv2_bbs_content.setText(newStr);
        }
    }

    /**
     * @brief 当长时间按下此控件时才会触发该方法
     *
     * @param v 事件源控件
     * @return 该方法的返回值为一个boolean类型的变量
     *      当返回true时，表示已经完整地处理了这个事件，并不希望其他的回调方法再次进行处理；
     *      当返回false时，表示并没有完全处理完该事件，更希望其他方法继续对其进行处理。
     */
    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.tv1_bbs_control || v.getId() == R.id.tv2_bbs_content) {
            // 清空文本视图tv2_bbs_content的文本内容
            tv2_bbs_content.setText("");
        }

        return true;
    }
}
