package com.lsgx.junior;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ClickActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click);
        // 从布局文件中获取名叫btn1_click的按钮控件
        Button btn1_click = findViewById(R.id.btn1_click);
        // 给btn1_click设置点击监听器，一旦用户点击按钮，就触发监听器的onClick方法
        btn1_click.setOnClickListener(new MyOnClickListener());
        // 给btn1_click设置长按监听器，一旦用户长按按钮，就触发监听器的onLongClick方法
        btn1_click.setOnLongClickListener(new MyOnLongClickListener());
    }

    // 定义一个点击监听器，它实现了接口View.OnClickListener
    class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) { // 点击事件的处理方法
            if (v.getId() == R.id.btn1_click) { // 判断是否为btn1_click被点击
                Toast.makeText(ClickActivity.this, "您点击了控件 : " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // 定义一个长按监听器，它实现了接口View.OnLongClickListener
    class MyOnLongClickListener implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) { // 长按事件的处理方法
            if (v.getId() == R.id.btn1_click) { // 判断是否为btn1_click被点击
                Toast.makeText(ClickActivity.this, "您长按了控件 : " + ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    }
}
