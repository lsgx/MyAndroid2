package com.lsgx.junior;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class ShapeActivity extends AppCompatActivity implements View.OnClickListener {

    private View v1_shape_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shape);

        v1_shape_content = findViewById(R.id.v1_shape_content);
        findViewById(R.id.btn1_shape_rect).setOnClickListener(this);
        findViewById(R.id.btn1_shape_oval).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) { // 一旦监听到点击动作，就触发监听器的onClick方法
        int viewId = v.getId();
        switch (viewId) {
            case R.id.btn1_shape_rect: {
                // 把矩形形状设置为v1_shape_content的背景
                v1_shape_content.setBackgroundResource(R.drawable.shape_rect_gold);
                break;
            }
            case R.id.btn1_shape_oval: {
                // 把椭圆形状设置为v1_shape_content的背景
                v1_shape_content.setBackgroundResource(R.drawable.shape_oval_rose);
                break;
            }
            default: {
                Log.w("IconActivity", "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }

}
