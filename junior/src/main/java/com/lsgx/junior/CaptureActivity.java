package com.lsgx.junior;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.junior.util.DateUtil;

public class CaptureActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnLongClickListener {

    private TextView tv1_capture; // 声明一个文本视图对象
    private ImageView iv1_capture; // 声明一个图像视图对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        // 从布局文件中获取名叫tv1_capture的文本视图
        tv1_capture = findViewById(R.id.tv1_capture);
        // 设置tv1_capture内部文本的移动方式为滚动形式
        // 如果不设置将无法拉动文本
        tv1_capture.setMovementMethod(new ScrollingMovementMethod());
        // 开启文本视图tv1_capture的绘图缓存 (默认不启用绘图缓存, 故需要手动设置启用)
        tv1_capture.setDrawingCacheEnabled(true);
        // 从布局文件中获取名叫iv1_capture的图像视图
        iv1_capture = findViewById(R.id.iv1_capture);
        // 从布局文件中获取名叫btn1_capture_chat的按钮
        Button btn1_capture_chat = findViewById(R.id.btn1_capture_chat);
        // 从布局文件中获取名叫btn2_capture_exec的按钮
        Button btn2_capture_exec = findViewById(R.id.btn2_capture_exec);
        // 给btn1_capture_chat设置点击监听器
        btn1_capture_chat.setOnClickListener(this);
        // 给btn1_capture_chat设置长按监听器
        btn1_capture_chat.setOnLongClickListener(this);
        // 给btn2_capture_exec设置点击监听器
        btn2_capture_exec.setOnClickListener(this);
    }

    private String[] mStrArray = {
            "你吃饭了吗？",
            "今天天气真好呀。",
            "我中奖啦！",
            "我们去看电影吧",
            "晚上干什么好呢？",
    };

    @Override
    public void onClick(View v) { // 一旦监听到点击动作，就触发监听器的onClick方法
        if (v.getId() == R.id.btn1_capture_chat) {
            // 生成一个0到4之间的随机数
            int number = (int) (Math.random() * 10) % 5;
            // 拼接聊天的文本内容
            String newStr = String.format("%s\n%s %s",
                    tv1_capture.getText().toString(), DateUtil.getNowTime(), mStrArray[number]);
            // 设置文本视图tv1_capture的文本内容
            tv1_capture.setText(newStr);
        } else if (v.getId() == R.id.btn2_capture_exec) { // 点击了截图按钮，则将截图信息显示在图像视图上
            // 从文本视图tv1_capture的绘图缓存中获取位图对象
            // 使用getDrawingCache方法时，会先自动去调用buildDrawingCache方法建立DrawingCache，再将结果返回。
            Bitmap bitmap = tv1_capture.getDrawingCache();
            // 给图像视图iv1_capture设置位图对象
            // 位图对象默认的格式是Bitmap.Config.ARGB_8888
            iv1_capture.setImageBitmap(bitmap);
            // 注意这里在截图完毕后不能马上关闭绘图缓存，因为画面渲染需要时间，
            // 如果立即关闭缓存，渲染画面就会找不到位图对象，会报错：
            // “java.lang.IllegalArgumentException: Cannot draw recycled bitmaps”。
            // 所以要等界面渲染完成后再关闭绘图缓存，下面的做法是延迟200毫秒再关闭
            mHandler.postDelayed(mResetCache, 200);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.btn1_capture_chat) {
            // 清空文本视图tv1_capturet的文本内容
            tv1_capture.setText("");
        }

        return true;
    }

    private Handler mHandler = new Handler(); // 声明一个任务处理器
    private Runnable mResetCache = new Runnable() {
        @Override
        public void run() { // 通过先禁用后启用View的绘图缓存, 达到强制刷新绘图缓存的作用
            // 禁用文本视图tv1_capture的绘图缓存
            tv1_capture.setDrawingCacheEnabled(false);
            // 启用文本视图tv1_capture的绘图缓存
            tv1_capture.setDrawingCacheEnabled(true);
        }
    };
}
