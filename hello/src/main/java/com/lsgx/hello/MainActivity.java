package com.lsgx.hello;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 当前页面的布局采用的是res/layout/activity_main.xml
        // see app/build/generated/not_namespaced_r_class_sources/debug/r/com/lsgx/hello/R.java
        setContentView(R.layout.activity_main);
        // 获取名叫tv1_hello的TextView控件
        TextView tv1_hello = findViewById(R.id.tv1_hello);
        // 设置TextView控件的文字内容
        tv1_hello.setText(getResources().getString(R.string.tv1_hello2));
        // 设置TextView控件的文字颜色
        tv1_hello.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTV1Green));
        // 设置TextView控件的文字大小
        tv1_hello.setTextSize(30F);

        // 动态创建TextView
        TextView tv2_hello = new TextView(getApplicationContext());
        tv2_hello.setId(R.id.tv2_hello);
        tv2_hello.setText("嘻嘻哈哈");
        tv2_hello.setTextColor(Color.RED);
        tv2_hello.setTextSize(36F);
        // 添加View到ViewGroup布局中
        ConstraintLayout main_layout = findViewById(R.id.main_layout);
        main_layout.addView(tv2_hello);
        // 动态约束修改
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.constrainHeight(tv2_hello.getId(), ConstraintSet.WRAP_CONTENT);
        constraintSet.constrainWidth(tv2_hello.getId(), constraintSet.WRAP_CONTENT);
        constraintSet.connect(tv2_hello.getId(), ConstraintSet.TOP, tv1_hello.getId(), constraintSet.BOTTOM);
        constraintSet.connect(tv2_hello.getId(), ConstraintSet.LEFT, tv1_hello.getId(), constraintSet.LEFT);
        // 在布局中应用约束条件
        constraintSet.applyTo(main_layout);
    }
}
