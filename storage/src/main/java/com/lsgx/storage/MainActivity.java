package com.lsgx.storage;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_share_write).setOnClickListener(this);
        findViewById(R.id.btn_share_read).setOnClickListener(this);
        findViewById(R.id.btn_login_share).setOnClickListener(this);
        findViewById(R.id.btn_sqlite_create).setOnClickListener(this);
        findViewById(R.id.btn_sqlite_write).setOnClickListener(this);
        findViewById(R.id.btn_sqlite_read).setOnClickListener(this);
        findViewById(R.id.btn_login_sqlite).setOnClickListener(this);
        findViewById(R.id.btn_file_basic).setOnClickListener(this);
        findViewById(R.id.btn_file_path).setOnClickListener(this);
        findViewById(R.id.btn_text_write).setOnClickListener(this);
        findViewById(R.id.btn_text_read).setOnClickListener(this);
        findViewById(R.id.btn_image_write).setOnClickListener(this);
        findViewById(R.id.btn_image_read).setOnClickListener(this);
        findViewById(R.id.btn_app_life).setOnClickListener(this);
        findViewById(R.id.btn_app_write).setOnClickListener(this);
        findViewById(R.id.btn_app_read).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId)
        {
            case R.id.btn_share_write :
            {
                Intent intent = new Intent(this, ShareWriteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_share_read :
            {
                Intent intent = new Intent(this, ShareReadActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_login_share :
            {
                Intent intent = new Intent(this, LoginShareActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_sqlite_create :
            {
                Intent intent = new Intent(this, DatabaseActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_sqlite_write :
            {
                Intent intent = new Intent(this, SQLiteWriteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_sqlite_read :
            {
                Intent intent = new Intent(this, SQLiteReadActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_login_sqlite :
            {
                Intent intent = new Intent(this, LoginSQLiteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_file_basic :
            {
                Intent intent = new Intent(this, FileBasicActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_file_path :
            {
                Intent intent = new Intent(this, FilePathActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_text_write :
            {
                Intent intent = new Intent(this, TextWriteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_text_read :
            {
                Intent intent = new Intent(this, TextReadActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_image_write :
            {
                Intent intent = new Intent(this, ImageWriteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_image_read :
            {
                Intent intent = new Intent(this, ImageReadActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_app_life :
            {
                Intent intent = new Intent(this, ActJumpActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_app_write :
            {
                Intent intent = new Intent(this, AppWriteActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_app_read :
            {
                Intent intent = new Intent(this, AppReadActivity.class);
                startActivity(intent);
                break;
            }
            default :
            {
                Log.w("MainActivity", "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }

}
