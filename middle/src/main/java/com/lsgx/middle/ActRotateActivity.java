package com.lsgx.middle;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.lsgx.middle.util.DateUtil;

public class ActRotateActivity extends AppCompatActivity {

    /**
     * onCreate : 创建activity时调用。设置在该方法中，还以Bundle中可以提出用于创建该 Activity 所需的信息
     * onStart : activity变为在屏幕上对用户可见时，即获得焦点时，会调用
     * onResume : activity开始与用户交互时调用（无论是启动还是重新启动一个活动，该方法总是被调用的）
     * onPause : activity被暂停或收回cpu和其他资源时调用，该方法用于保存活动状态的
     * onStop : activity被停止并转为不可见阶段及后续的生命周期事件时，即失去焦点时调用
     * onDestroy : activity被完全从系统内存中移除时调用，该方法被调用可能是因为有人直接调用 finish()方法 或者系统决定停止该活动以释放资源
     *
     * onSaveInstanceState : 不是生命周期方法，只有在由系统销毁一个Activity时，会被调用
     * onRestoreInstanceState : 不是生命周期方法，只有在activity被系统回收，重新创建activity的情况下才会被调用
     * onConfigurationChanged : 不是生命周期方法，当系统的配置信息发生改变时，系统会调用此方法
     *
     */

    private final static String TAG = "ActRotateActivity";
    private TextView tv_life; // 声明一个文本视图对象
    private Button btn_screen_switch;
    private String mStr = "";

    private void refreshLife(String desc) { // 刷新生命周期的日志信息
        Log.d(TAG, desc);
        mStr = String.format("%s%s %s %s\n", mStr, DateUtil.getNowTimeDetail(), TAG, desc);
        tv_life.setText(mStr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { // 创建活动页面
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_rotate);
        // 从布局文件中获取名叫tv_life的文本视图
        tv_life = findViewById(R.id.tv_life);
        tv_life.setMovementMethod(new ScrollingMovementMethod());
        refreshLife("onCreate");

        btn_screen_switch = findViewById(R.id.btn_screen_switch);
        initConfigChange();
        btn_screen_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    // 切换屏幕状态为横屏
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                } else {
                    // 切换屏幕状态为竖屏
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) { // 系统配置属性改变
        refreshLife("onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
        initConfigChange();
    }

    private void initConfigChange() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // 当前屏幕状态为竖屏
            refreshLife("current screen is orientation portrait");
            btn_screen_switch.setText("切换屏幕状态为横屏");
        } else {
            // 当前屏幕状态为横屏
            refreshLife("current screen is orientation landscape");
            btn_screen_switch.setText("切换屏幕状态为竖屏");
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        refreshLife("onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        refreshLife("onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() { // 开始活动页面
        refreshLife("onStart");
        super.onStart();
    }

    @Override
    protected void onStop() { // 停止活动页面
        refreshLife("onStop");
        super.onStop();
    }

    @Override
    protected void onResume() { // 恢复活动页面
        refreshLife("onResume");
        super.onResume();
    }

    @Override
    protected void onPause() { // 暂停活动页面
        refreshLife("onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() { // 重启活动页面
        refreshLife("onRestart");
        super.onRestart();
        initConfigChange();
    }

    @Override
    protected void onDestroy() { // 销毁活动页面
        refreshLife("onDestroy");
        super.onDestroy();
    }

}
