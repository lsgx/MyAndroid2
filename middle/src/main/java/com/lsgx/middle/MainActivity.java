package com.lsgx.middle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_chapter_3_1_demo_relative_xml).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_1_demo_relative_code).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_1_demo_frame).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_2_demo_checkbox).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_2_demo_switch_default).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_2_demo_switch_ios).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_2_demo_radio_horizontal).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_2_demo_radio_vertical).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_3_demo_spinner_dropdown).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_3_demo_spinner_dialog).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_3_demo_spinner_icon).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_simple).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_cursor).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_border).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_hide).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_jump).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_4_demo_edit_auto).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_5_demo_act_jump).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_5_demo_act_rotate).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_5_demo_act_home).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_5_demo_act_uri).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_5_demo_act_request).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_6_project_text_check).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_6_project_mortgage_calc).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_7_project_alert_dialog).setOnClickListener(this);
        findViewById(R.id.btn_chapter_3_7_project_app_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId)
        {
            case R.id.btn_chapter_3_1_demo_relative_xml :
            {
                Intent intent = new Intent(this, RelativeXmlActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_1_demo_relative_code :
            {
                Intent intent = new Intent(this, RelativeCodeActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_1_demo_frame :
            {
                Intent intent = new Intent(this, FrameActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_2_demo_checkbox :
            {
                Intent intent = new Intent(this, CheckboxActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_2_demo_switch_default :
            {
                Intent intent = new Intent(this, SwitchDefaultActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_2_demo_switch_ios :
            {
                Intent intent = new Intent(this, SwitchIOSActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_2_demo_radio_horizontal :
            {
                Intent intent = new Intent(this, RadioHorizontalActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_2_demo_radio_vertical :
            {
                Intent intent = new Intent(this, RadioVerticalActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_3_demo_spinner_dropdown :
            {
                Intent intent = new Intent(this, SpinnerDropdownActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_3_demo_spinner_dialog :
            {
                Intent intent = new Intent(this, SpinnerDialogActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_3_demo_spinner_icon :
            {
                Intent intent = new Intent(this, SpinnerIconActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_simple :
            {
                Intent intent = new Intent(this, EditSimpleActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_cursor :
            {
                Intent intent = new Intent(this, EditCursorActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_border :
            {
                Intent intent = new Intent(this, EditBorderActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_hide :
            {
                Intent intent = new Intent(this, EditHideActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_jump :
            {
                Intent intent = new Intent(this, EditJumpActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_4_demo_edit_auto :
            {
                Intent intent = new Intent(this, EditAutoActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_5_demo_act_jump :
            {
                Intent intent = new Intent(this, ActJumpActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_5_demo_act_rotate :
            {
                Intent intent = new Intent(this, ActRotateActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_5_demo_act_home :
            {
                Intent intent = new Intent(this, ActHomeActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_5_demo_act_uri :
            {
                Intent intent = new Intent(this, ActUriActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_5_demo_act_request :
            {
                Intent intent = new Intent(this, ActRequestActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_6_project_text_check :
            {
                Intent intent = new Intent(this, TextCheckActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_6_project_mortgage_calc :
            {
                Intent intent = new Intent(this, MortgageActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_7_project_alert_dialog :
            {
                Intent intent = new Intent(this, AlertActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_chapter_3_7_project_app_login :
            {
                Intent intent = new Intent(this, LoginMainActivity.class);
                startActivity(intent);
                break;
            }
            default :
            {
                Log.w("MainActivity", "In click event, unknown view id is : " + viewId);
                break;
            }
        }
    }
}
