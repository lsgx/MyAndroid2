# MyAndroid2 

MyAndroid2 是我个人学习 《 Android Studio开发实战：从零基础到App上线（第2版） 》 的练习代码

## 练习环境
- Android Studio 3.5
- Android API 29 ( Android 10.0 )

## Android Studio
![androidstudio](screen/android_studio.png)

## Android 开发参考链接
- [Android 开发者首页](https://developer.android.google.cn)
- [Android Studio 下载首页](https://developer.android.google.cn/studio)
- [Android Studio 用户指南](https://developer.android.google.cn/studio/intro)
- [Android 开发者指南](https://developer.android.google.cn/guide)
- [android 参考文档](https://developer.android.google.cn/docs)
- [Android API 参考](https://developer.android.google.cn/reference)
- [Android 示例参考](https://developer.android.google.cn/samples)

## 随书源码
- [github android2](https://github.com/aqi00/android2)

## 作者博客
- [csdn aqi00](https://blog.csdn.net/aqi00)

## 书籍封面
![MainActivity](screen/wr_bookCover_img.png)

## 书籍目录列表
```text
版权信息
作者简介
内容简介
插图
推荐序
再版前言
第一版前言
第1章 Android Studio环境搭建
1.1 Android Studio简介
1.2 Android Studio的安装
1.3 运行小应用Hello World
1.4 App的工程结构
1.5 准备开始
1.6 小 结
第2章 初级控件
2.1 屏幕显示
2.2 简单布局
2.3 简单控件
2.4 图形基础
2.5 实战项目：简单计算器
2.6 小 结
第3章 中级控件
3.1 其他布局
3.2 特殊按钮
3.3 适配视图基础
3.4 编辑框
3.5 活动Activity基础
3.6 实战项目：房贷计算器
3.7 实战项目：登录App
3.8 小 结
第4章 数据存储
4.1 共享参数SharedPreferences
4.2 数据库SQLite
4.3 SD卡文件操作
4.4 应用Application基础
4.5 内容提供与处理
4.6 实战项目：购物车
4.7 小 结
第5章 高级控件
5.1 日期时间控件
5.2 列表类视图
5.3 翻页类视图
5.4 碎片Fragment
5.5 广播Broadcast基础
5.6 实战项目：万年历
5.7 实战项目：日程表
5.8 小 结
第6章 自定义控件
6.1 自定义视图
6.2 自定义动画
6.3 自定义对话框
6.4 自定义通知栏
6.5 服务Service基础
6.6 实战项目：手机安全助手
6.7 小 结
第7章 组合控件
7.1 标签栏
7.2 导航栏
7.3 横幅条
7.4 增强型列表
7.5 材质设计库
7.6 实战项目：仿支付宝的头部伸缩特效
7.7 实战项目：仿淘宝主页
7.8 小 结
第8章 调试与上线
8.1 调试工作
8.2 准备上线
8.3 安全加固
8.4 发布到应用商店
8.5 小 结
第9章 设备操作
9.1 摄像头
9.2 麦克风
9.3 传感器
9.4 手机定位
9.5 短距离通信
9.6 实战项目：仿微信的发现功能
9.7 小 结
第10章 网络通信
10.1 多线程
10.2 HTTP接口访问
10.3 上传和下载
10.4 套接字Socket
10.5 实战项目：仿应用宝的应用更新功能
10.6 实战项目：仿手机QQ的聊天功能
10.7 小 结
第11章 事 件
11.1 按键事件
11.2 触摸事件
11.3 手势检测
11.4 手势冲突处理
11.5 实战项目：抠图神器——美图变变
11.6 实战项目：虚拟现实的全景图库
11.7 小 结
第12章 动 画
12.1 帧动画
12.2 补间动画
12.3 属性动画
12.4 矢量动画
12.5 动画的实现手段
12.6 实战项目：仿QQ空间的动感影集
12.7 小 结
第13章 多媒体
13.1 相册
13.2 音频播放
13.3 视频播放
13.4 多窗口
13.5 实战项目：影视播放器——爱看剧场
13.6 实战项目：音乐播放器——浪花音乐
13.7 小 结
第14章 融合技术
14.1 网页集成
14.2 JNI开发
14.3 局域网共享
14.4 实战项目：共享经济弄潮儿——WiFi共享器
14.5 实战项目：笔墨飘香之电子书架
14.6 小 结
第15章 第三方开发包
15.1 地图SDK
15.2 分享SDK
15.3 支付SDK
15.4 语音SDK
15.5 实战项目：仿滴滴打车
15.6 小 结
第16章 性能优化
16.1 布局文件优化
16.2 内存泄漏处理
16.3 线程池管理
16.4 省电模式
16.5 实战项目：网络图片缓存框架
16.6 小 结
附 录
附录一 仿流行App的常用功能
附录二 Android各版本的新增功能说明
附录三 手机硬件与App开发的关联
附录四 专业术语索引
```
